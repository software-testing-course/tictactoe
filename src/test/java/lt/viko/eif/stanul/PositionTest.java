package lt.viko.eif.stanul;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class PositionTest {

    @Test
    public void testIfStartingShapeIsSelectedProperly() {
        Position positionWithX = new Position(3, "x");
        Position positionWithO = new Position(4, "o");
        assertEquals('x', positionWithX.turn);
        assertNotEquals('o', positionWithX.turn);
        assertEquals('o', positionWithO.turn);
        assertNotEquals('x', positionWithO.turn);
    }

    @Test
    public void testIfBoardSizeMatchesSpecifiedDimensions() {
        Position positionWithSize3 = new Position(3, "x");
        Position positionWithSize4 = new Position(4, "x");
        assertEquals('x', positionWithSize3.turn);
        assertEquals(Arrays.deepToString(new char[][]
                {
                        {' ', ' ', ' '},
                        {' ', ' ', ' '},
                        {' ', ' ', ' '}
                }), positionWithSize3.toString());
        assertEquals(Arrays.deepToString(new char[][]
                {
                        {' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' '}
                }), positionWithSize4.toString());
    }

    @Test
    public void testIfMoveIsPerformed() {
        Position positionWithSize3 = new Position(3, "x").move(1, 2);
        Position positionWithSize4 = new Position(4, "x").move(2, 3);
        assertEquals('o', positionWithSize3.turn);
        assertNotEquals(Arrays.deepToString(new char[][]
                {
                        {' ', ' ', 'x'},
                        {' ', ' ', ' '},
                        {' ', ' ', ' '}
                }), positionWithSize3.toString());
        assertEquals(Arrays.deepToString(new char[][]
                {
                        {' ', ' ', ' '},
                        {' ', ' ', 'x'},
                        {' ', ' ', ' '}
                }), positionWithSize3.toString());
        assertEquals(Arrays.deepToString(new char[][]
                {
                        {' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', 'x'},
                        {' ', ' ', ' ', ' '}
                }), positionWithSize4.toString());
    }

    @Test
    public void testIsWinMethodHorizontal() {
        char[][] testBoardVertical1st3by3 = new char[][]
                {
                        {'x', ' ', ' '},
                        {'x', ' ', ' '},
                        {'x', ' ', ' '}
                };
        char[][] testBoardVertical2nd3by3 = new char[][]
                {
                        {'x', ' ', ' '},
                        {'x', ' ', ' '},
                        {'o', ' ', ' '}
                };
        char[][] testBoardVertical3rd3by3 = new char[][]
                {
                        {' ', 'o', ' '},
                        {' ', 'o', ' '},
                        {' ', 'o', ' '}
                };
        char[][] testBoardVertical1st4by4 = new char[][]
                {
                        {' ', ' ', ' ', ' '},
                        {' ', ' ', 'x', ' '},
                        {' ', ' ', 'x', ' '},
                        {' ', ' ', 'x', ' '}
                };
        assertFalse(new Position(3, "x").isWinFor('x'));
        assertTrue(new Position(3, testBoardVertical1st3by3, 'x').isWinFor('x'));
        assertFalse(new Position(3, testBoardVertical2nd3by3, 'x').isWinFor('x'));
        assertTrue(new Position(3, testBoardVertical3rd3by3, 'o').isWinFor('o'));
        assertFalse(new Position(3, testBoardVertical3rd3by3, 'o').isWinFor('x'));
        assertTrue(new Position(4, testBoardVertical1st4by4, 'o').isWinFor('x'));
        assertFalse(new Position(4, testBoardVertical1st4by4, 'o').isWinFor('o'));
    }

    @Test
    public void testIsWinMethodVertical() {
        char[][] testBoardVertical1st3by3 = new char[][]
                {
                        {'x', 'x', 'x'},
                        {' ', ' ', ' '},
                        {' ', ' ', ' '}
                };
        char[][] testBoardVertical2nd3by3 = new char[][]
                {
                        {'x', 'x', 'o'},
                        {' ', ' ', ' '},
                        {' ', ' ', ' '}
                };
        char[][] testBoardVertical3rd3by3 = new char[][]
                {
                        {' ', ' ', ' '},
                        {'o', 'o', 'o'},
                        {' ', ' ', ' '}
                };
        char[][] testBoardVertical1st4by4 = new char[][]
                {
                        {' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' '},
                        {' ', 'x', 'x', 'x'},
                        {' ', ' ', ' ', ' '}
                };
        assertFalse(new Position(3, "x").isWinFor('x'));
        assertTrue(new Position(3, testBoardVertical1st3by3, 'x').isWinFor('x'));
        assertFalse(new Position(3, testBoardVertical2nd3by3, 'x').isWinFor('x'));
        assertTrue(new Position(3, testBoardVertical3rd3by3, 'o').isWinFor('o'));
        assertFalse(new Position(3, testBoardVertical3rd3by3, 'o').isWinFor('x'));
        assertTrue(new Position(4, testBoardVertical1st4by4, 'o').isWinFor('x'));
        assertFalse(new Position(4, testBoardVertical1st4by4, 'o').isWinFor('o'));
    }

    @Test
    public void testIsWinMethodDiagonal() {
        char[][] testBoardVertical1st3by3 = new char[][]
                {
                        {'x', ' ', ' '},
                        {' ', 'x', ' '},
                        {' ', ' ', 'x'}
                };
        char[][] testBoardVertical2nd3by3 = new char[][]
                {
                        {'x', ' ', ' '},
                        {' ', 'o', ' '},
                        {' ', ' ', 'x'}
                };
        char[][] testBoardVertical3rd3by3 = new char[][]
                {
                        {'o', ' ', ' '},
                        {' ', 'o', ' '},
                        {' ', ' ', 'o'}
                };
        char[][] testBoardVertical1st4by4 = new char[][]
                {
                        {' ', ' ', ' ', ' '},
                        {' ', 'x', ' ', ' '},
                        {' ', ' ', 'x', ' '},
                        {' ', ' ', ' ', 'x'}
                };
        char[][] testBoardVertical1st7by7 = new char[][]
                {
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                        {' ', 'o', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', 'o', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', 'o', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                };
        assertFalse(new Position(3, "x").isWinFor('x'));
        assertTrue(new Position(3, testBoardVertical1st3by3, 'x').isWinFor('x'));
        assertFalse(new Position(3, testBoardVertical2nd3by3, 'x').isWinFor('x'));
        assertTrue(new Position(3, testBoardVertical3rd3by3, 'o').isWinFor('o'));
        assertFalse(new Position(3, testBoardVertical3rd3by3, 'o').isWinFor('x'));
        assertTrue(new Position(4, testBoardVertical1st4by4, 'o').isWinFor('x'));
        assertFalse(new Position(4, testBoardVertical1st4by4, 'o').isWinFor('o'));
        assertTrue(new Position(7, testBoardVertical1st7by7, 'o').isWinFor('o'));
    }

    @Test
    public void testIsWinMethodAntiDiagonal() {
        char[][] testBoardVertical1st3by3 = new char[][]
                {
                        {' ', ' ', 'x'},
                        {' ', 'x', ' '},
                        {'x', ' ', ' '}
                };
        char[][] testBoardVertical2nd3by3 = new char[][]
                {
                        {' ', ' ', 'x'},
                        {' ', 'o', ' '},
                        {'x', ' ', ' '}
                };
        char[][] testBoardVertical3rd3by3 = new char[][]
                {
                        {' ', ' ', 'o'},
                        {' ', 'o', ' '},
                        {'o', ' ', ' '}
                };
        char[][] testBoardVertical1st4by4 = new char[][]
                {
                        {' ', ' ', ' ', ' '},
                        {' ', ' ', 'x', ' '},
                        {' ', 'x', ' ', ' '},
                        {'x', ' ', ' ', ' '}
                };
        char[][] testBoardVertical1st7by7 = new char[][]
                {
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', 'o', ' ', ' ', ' '},
                        {' ', ' ', 'o', ' ', ' ', ' ', ' '},
                        {' ', 'o', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                };
        assertFalse(new Position(3, "x").isWinFor('x'));
        assertTrue(new Position(3, testBoardVertical1st3by3, 'x').isWinFor('x'));
        assertFalse(new Position(3, testBoardVertical2nd3by3, 'x').isWinFor('x'));
        assertTrue(new Position(3, testBoardVertical3rd3by3, 'o').isWinFor('o'));
        assertFalse(new Position(3, testBoardVertical3rd3by3, 'o').isWinFor('x'));
        assertTrue(new Position(4, testBoardVertical1st4by4, 'o').isWinFor('x'));
        assertFalse(new Position(4, testBoardVertical1st4by4, 'o').isWinFor('o'));
        assertTrue(new Position(7, testBoardVertical1st7by7, 'o').isWinFor('o'));
    }

    @Test
    public void testIsGameOver() {
        char[][] boardWithWinAsX = new char[][]
                {
                        {' ', ' ', 'x'},
                        {' ', 'x', ' '},
                        {'x', ' ', ' '}
                };
        char[][] boardWithWinAsO = new char[][]
                {
                        {' ', 'o', 'x'},
                        {' ', 'o', ' '},
                        {'x', 'o', ' '}
                };
        char[][] boardWithWinNoWinCondition = new char[][]
                {
                        {' ', ' ', 'o'},
                        {' ', 'x', 'x'},
                        {'o', ' ', 'o'}
                };
        char[][] boardWithDraw = new char[][]
                {
                        {'x', 'o', 'x'},
                        {'o', 'x', 'o'},
                        {'o', 'x', 'o'}
                };
        char[][] boardBigWithWin = new char[][]
                {
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', 'o', ' ', ' ', ' '},
                        {' ', ' ', 'o', ' ', ' ', ' ', ' '},
                        {' ', 'o', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                };
        char[][] boardBigWithoutAnything = new char[][]
                {
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', 'o', ' ', 'x', ' ', ' '},
                        {' ', 'x', ' ', ' ', ' ', ' ', ' '},
                        {' ', ' ', ' ', 'o', 'o', ' ', ' '},
                        {' ', ' ', 'x', ' ', ' ', ' ', ' '},
                        {' ', 'o', ' ', ' ', ' ', 'x', ' '},
                        {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                };
        assertFalse(new Position(3, "x").isGameOver());
        assertTrue(new Position(3, boardWithWinAsX, 'x').isGameOver());
        assertTrue(new Position(3, boardWithWinAsO, 'x').isGameOver());
        assertFalse(new Position(3, boardWithWinNoWinCondition, 'x').isGameOver());
        assertTrue(new Position(3, boardWithDraw, 'x').isGameOver());
        assertTrue(new Position(7, boardBigWithWin, 'x').isGameOver());
        assertFalse(new Position(7, boardBigWithoutAnything, 'x').isGameOver());
    }

}