package lt.viko.eif.stanul;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Scanner;

public class Game extends JFrame {

    public Game(int dim, String startingShape) {

        Position position = new Position(dim, startingShape);

        setLayout(new GridLayout(dim, dim));
        for (int i = 0; i < dim; i++) {
            final int yIndex = i;
            for (int j = 0; j < dim; j++) {
                final int xIndex = j;
                JButton button = createButton();
                button.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (position.boardSquared[xIndex][yIndex] == ' ') {
                            button.setText(Character.toString(position.turn));
                            position.move(xIndex, yIndex);
                        }
                        if (position.isGameOver()) {
                            playSound("applause_y.wav");
                        }
                    }
                });

            }
        }
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Board size and starting shape?");
        int boardSize = scanner.nextInt();
        String staringShape = scanner.next();

        SwingUtilities.invokeLater(() -> new Game(boardSize, staringShape));
    }

    public static synchronized void playSound(final String url) {
        new Thread(() -> {
            try {
                Clip clip = AudioSystem.getClip();
                AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File(url));
                clip.open(inputStream);
                clip.start();
            } catch (Exception e) {
                System.err.println("Exception: " + e.getMessage());
            }
        }).start();
    }

    private JButton createButton() {
        JButton button = new JButton();
        button.setPreferredSize(new Dimension(100, 100));
        button.setBackground(Color.BLACK);
        button.setOpaque(true);
        button.setFont(new Font("Comic Sans MS", Font.PLAIN, 50));
        add(button);
        return button;
    }

}
