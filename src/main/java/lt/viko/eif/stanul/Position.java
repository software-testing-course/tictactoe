package lt.viko.eif.stanul;

import javax.swing.*;
import java.util.Arrays;

public class Position {
    public static int dim;
    public int size;
    public char turn;
    public char[][] boardSquared;
    int howMuchInColumn = 0;
    int howMuchInRow = 0;

    public Position(int tmpDim, String tmpStartingShape) {
        turn = tmpStartingShape.charAt(0);
        dim = tmpDim;
        boardSquared = new char[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                boardSquared[j][i] = ' ';
            }
        }
    }

    public Position(int tmpDim, char[][] arrayOfShapes, char turn) {
        this.boardSquared = arrayOfShapes;
        this.turn = turn;
        dim = tmpDim;
        size = tmpDim * tmpDim;
    }

    @Override
    public String toString() {
        return (Arrays.deepToString(boardSquared));
    }

    public Position move(int xIndex, int yIndex) {
        boardSquared[xIndex][yIndex] = turn;
        turn = turn == 'x' ? 'o' : 'x';
        return this;
    }

    public boolean isWinFor(char turn) {
        for (int x = 0; x < dim; x++) {
            for (int i = 0; i < dim; i++) {
                howMuchInRow = (boardSquared[i][x] == turn) ? howMuchInRow + 1 : 0;
                howMuchInColumn = (boardSquared[x][i] == turn) ? howMuchInColumn + 1 : 0;
                if (howMuchInColumn == 3 || howMuchInRow == 3) {
                    return true;
                }
            }
        }

        for (int x = 0; x < dim - 2; x++) {
            for (int i = 0; i < dim - 2; i++) {
                if (boardSquared[x][i] == turn) {
                    if (boardSquared[x][i] == boardSquared[x + 1][i + 1] && boardSquared[x][i] == boardSquared[x + 2][i + 2])
                        return true;
                }
                if (boardSquared[x + 1][i + 1] == turn) {
                    if (boardSquared[x + 2][i] == boardSquared[x + 1][i + 1] && boardSquared[x + 2][i] == boardSquared[x][i + 2])
                        return true;
                }
            }
        }
        return false;
    }

    public int blanks() {
        int total = 0;
        for (int x = 0; x < dim; x++) {
            for (int i = 0; i < dim; i++) {
                if (boardSquared[x][i] == ' ') {
                    total++;
                }
            }
        }
        return total;
    }

    public boolean isGameOver() {
        if (isWinFor('x')) {
            JOptionPane.showMessageDialog(null, "X won");
        }
        if (isWinFor('o')) {
            JOptionPane.showMessageDialog(null, "O won");
        }
        if (blanks() == 0) {
            JOptionPane.showMessageDialog(null, "Draw");
        }

        return isWinFor('x') || isWinFor('o') || blanks() == 0;
    }
}
